package com.kenfogel.gopigo3driverproducer;

import com.kenfogel.gopigo3driver.GoPiGo3;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;

/**
 * Producer for GoPiGo3Driver so that the GoPiGo3 driver may be injected.
 *
 * @author Ken Fogel
 */
@ApplicationScoped
public class GoPiGo3DriverProducer {

    static final Logger LOG = Logger.getLogger(GoPiGo3DriverProducer.class.getName());

    /**
     * The producer of an injectable gopigo3 driver
     *
     * @return The driver
     */
    @Produces
    @Default
    @ApplicationScoped
    public GoPiGo3 createGoPiGo3Driver() {
        LOG.log(Level.INFO, "Instantiating GoPiGo driver");
        GoPiGo3 gopigo = null;
        try {
            gopigo = new GoPiGo3();
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Unable to instantiate GoPiGo", ex);
        }
        return gopigo;
    }
}
